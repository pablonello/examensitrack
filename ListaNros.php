<?php

class ListaNros {

    private $cantidadNros;
    private $nros;

    function __construct() {
        $this->nros = array();
    }

    public function llenarNros($cantNros) {
        for ($i = 0; $i < $cantNros; $i++) {
            array_push($this->nros, rand(-1000, 1000));
        }
        return $this->nros;
    }

    public function cantidadNros() {
        $this->cantidadNros = rand(0, 1000);
        return $this->cantidadNros;
    }

}
