<?php

class SumaPares {

    public $contarNros;

    function __construct() {
        
    }

    public function sumarParesLista($totalNros, $nro, $cantidadNros) {
        if ($nro < 0 && $nro > 1000) {
            throw new Exception('El nro debe estar en el ranzo entre 0 y 1000');
        } else {
            if (empty($totalNros)) {
                throw new Exception('El arreglo no puede estar vacio');
            } else if ($cantidadNros < 2) {
                throw new Exception('El arreglo debe contener al menos 2 numeros');
            } else {
                for ($i = 0; $i < count($totalNros); $i++) {
                    $segundoNro = $i + 1;
                    for ($j = $segundoNro; $j < count($totalNros); $j++) {
                        $suma = $totalNros[$i] + $totalNros[$j];
                        if ($suma == $nro) {
                            $this->contarNros++;
                        }
                    }
                }

                return $this->contarNros;
            }
        }
    }

    public function buscarPares($totalNros, $nro) {
        $arr = array(array());
        for ($i = 0; $i < count($totalNros); $i++) {
            $segundoNro = $i + 1;
            for ($j = $segundoNro; $j < count($totalNros); $j++) {
                $suma = $totalNros[$i] + $totalNros[$j];
                if ($suma == $nro) {
                    print '['. $totalNros[$i].','.$totalNros[$j].']';
                }
            }
        }

        return $this->contarNros;
    }

}
