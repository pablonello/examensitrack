<!DOCTYPE html> 
<html lang="es"> 
    <head> 
        <meta charset="utf-8"> 
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Examen Sitrack</title> 
    </head> 
    <?php
    //incluyo a los objetos a utilizar
    include 'ListaNros.php';
    include 'Numero.php';
    include 'SumaPares.php';
    ?>
    <body> 
        <?php
        $listaNros = new ListaNros();
        $cantNroNueva = $listaNros->cantidadNros();
        $listaNrosNueva = $listaNros->llenarNros($cantNroNueva);

        $nro = new Numero();
        $nroNuevo = $nro->generarNro();

        $sumoPares = new SumaPares();

        $contarPares = $sumoPares->sumarParesLista($listaNrosNueva, $nroNuevo, $cantNroNueva);



        if (empty($contarPares)) {
            print '<strong>';
            print 'Del conjunto total de ' . $cantNroNueva . ' elementos ';
            print '</strong>';
            print'<br>';
            print_r($listaNrosNueva);
            print'<br>';
            print '<br>';
            print '<br>';
            print '<strong>';
            print ' Hay 0 pares de elementos cuya suma es ' . $nroNuevo;
            print '</strong>';
        } else {
            print '<strong>';
            print 'Del conjunto total de ' . $cantNroNueva . ' elementos ';
            print '</strong>';
            print'<br>';
            print_r($listaNrosNueva);
            print'<br>';
            print '<br>';
            print '<br>';
            print '<strong>';
            print ' Hay ' . $contarPares . ' pares de elementos cuya suma es ' . $nroNuevo;
            print ' y los pares son: ';
            $buscarPares = $sumoPares->buscarPares($listaNrosNueva, $nroNuevo);
            print '</strong>';
        }
        ?>

    </body> 
</html> 