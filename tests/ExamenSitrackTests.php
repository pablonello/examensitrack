<?php

use PHPUnit\Framework\TestCase;

class ExamenSitrackTests extends TestCase {

    public function testCantidadNros() {
        $cantNros = new ListaNros();
        $this->assertNotEmpty($cantNros->cantidadNros(), "Se genero el número correctamente");
    }

    public function testArregloNros() {
        $arregloNros = new ListaNros();
        $cantNros = $arregloNros->cantidadNros();
        $this->assertNotEmpty($arregloNros->llenarNros($cantNros), "La lista esta llen de valores");
    }

    public function testContarPares() {
        $listaNros = new ListaNros();
        $cantNroNueva = $listaNros->cantidadNros();
        $listaNrosNueva = $listaNros->llenarNros($cantNroNueva);

        $nro = new Numero();
        $nroNuevo = $nro->generarNro();

        $sumoPares = new SumaPares();
        $this->assertNotEmpty($sumoPares->sumarParesLista($listaNrosNueva, $nroNuevo, $cantNroNueva));
    }

}
